## Blockland Installer
This is a very basic Bash script to download the latest files for Blockland.  It is assumed that you place this script into the directory you want to download the files to and run it from that directory.  The file `Blockland.exe` will end up next to this script in the directory.  It relies on `wget`.
