#!/bin/bash

mkdir temp-blobs
wget -nv -nc -O latestManifest.dat -U blocklandWIN/2.0 http://update.blockland.us/latestVersion.php || exit -1
IFS=$'\t'
read blank URL < latestManifest.dat

blobs=""
while read file blob; do blobs="$blobs $URL/$blob"; done <<< $(tail -n +2 latestManifest.dat)
echo $blobs | xargs -n 1 -P 20 wget -nv -N -P "./temp-blobs/" &&
echo "Copying blobs to proper locations..." &&
while read file blob; do mkdir -p $(dirname "./$file") && cp "./temp-blobs/$blob" "./$file"; done <<< $(tail -n +2 latestManifest.dat) &&
rm -rf ./temp-blobs/ &&
echo "Installation finished" ||
echo "Installation failed"
